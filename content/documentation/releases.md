---
title: "Releases & downloads"
date: 2020-03-02T10:42:48+01:00
draft: false
class: downloads
intro: "Download the library here!"
hide: true
---



## Releases


The library can be used as a npm package, or you can download the file you need on unpkg below. 
The polyfill will be perfect to use if you need to quickly set up a html to pdf on screen rendering.


| |                 |
|----------:|:--------------------|
| The library | [paged.js](https://unpkg.com/browse/pagedjs/dist/paged.js) |
| The polyfill | [paged.polyfill.js](https://unpkg.com/browse/pagedjs/dist/paged.polyfill.js) |
| ESM | [paged.esm.js](https://unpkg.com/browse/pagedjs/dist/paged.esm.js) |
| The library (legacy) | [paged.legacy.js](https://unpkg.com/browse/pagedjs/dist/paged.legacy.js) |
| The polyfill (legacy) | [paged.legacy.polyfill.js](https://unpkg.com/browse/pagedjs/dist/paged.legacy.polyfill.js) |

